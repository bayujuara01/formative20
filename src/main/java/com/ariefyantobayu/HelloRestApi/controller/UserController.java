package com.ariefyantobayu.HelloRestApi.controller;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.HelloRestApi.entity.User;
import com.ariefyantobayu.HelloRestApi.handler.ResponseHandler;
import com.ariefyantobayu.HelloRestApi.repository.UserRepository;
import com.ariefyantobayu.HelloRestApi.repository.UserRepositoryImpl;
import com.ariefyantobayu.HelloRestApi.response.DataResponse;
import com.ariefyantobayu.HelloRestApi.response.ErrorResponse;
import com.google.common.hash.Hashing;

@RestController
public class UserController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserRepositoryImpl userRepositoryImpl;

	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getAllUser() {
		List<User> users = userRepository.findAllNative();
		ResponseHandler response;

		if (users.size() > 0) {
			response = DataResponse.of(HttpStatus.OK, users);
		} else {
			response = DataResponse.of(HttpStatus.NO_CONTENT, new ArrayList<User>());
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getUserById(@PathVariable Long id) {
		Optional<User> user = userRepository.findByIdNative(id);
		ResponseHandler response;

		if (user.isPresent()) {
			response = DataResponse.of(HttpStatus.OK, user);
		} else {
			response = ErrorResponse.of(HttpStatus.NO_CONTENT, "Not found");
		}

		return ResponseEntity.ok(response);
	}

	@PostMapping(value = "/users")
	ResponseEntity<ResponseHandler> saveNewUser(@RequestBody User user) {
		String passwordHash = Hashing.sha256().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();

		Optional<User> userExist = userRepository.findByUsernameNative(user.getUsername());

		if (userExist.isEmpty()) {
			Long userReturnId = userRepositoryImpl.saveUserReturnNative(user.getUsername(), passwordHash);
			Optional<User> userRequest = userRepository.findByIdNative(userReturnId);
			return ResponseEntity.status(HttpStatus.CREATED).body(DataResponse.of(HttpStatus.CREATED, userRequest));
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Username is already in use"));
		}
	}

	@DeleteMapping("/users/{id}")
	ResponseEntity<ResponseHandler> deleteUserById(@PathVariable Long id) {
		Optional<User> userRequest = userRepository.findById(id);

		if (userRequest.isPresent()) {
			User userResult = userRequest.get();
			int deleteRowAffected = userRepository.deleteUserByIdNative(id);
			return ResponseEntity.status(HttpStatus.OK).body(DataResponse.of(HttpStatus.OK, userResult));
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Not Found", "User not found"));
		}

	}
}
