package com.ariefyantobayu.HelloRestApi.controller;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.HelloRestApi.entity.User;
import com.ariefyantobayu.HelloRestApi.handler.ResponseHandler;
import com.ariefyantobayu.HelloRestApi.repository.UserRepository;
import com.ariefyantobayu.HelloRestApi.response.DataResponse;
import com.ariefyantobayu.HelloRestApi.response.ErrorResponse;
import com.google.common.hash.Hashing;

@RestController
public class AuthController {

	@Autowired
	UserRepository userRepository;

	@PostMapping("/auth/login")
	ResponseEntity<ResponseHandler> userLogin(@RequestBody User user) {
		String passwordHashCode = Hashing.sha256().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();
		
		Optional<User> userRequest = userRepository.findByUsernameNative(user.getUsername());
		
		if (userRequest.isPresent()) {
			User userResult = userRequest.get();
			
			if (passwordHashCode.equals(userResult.getPassword())) {
				return ResponseEntity.status(HttpStatus.OK)
						.body(DataResponse.of(HttpStatus.OK, userResult));				
			} else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
						.body(ErrorResponse.of(HttpStatus.UNAUTHORIZED, "Not Authorized", "username or password are wrong"));
			}
		}  else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body(ErrorResponse.of(HttpStatus.UNAUTHORIZED, "Not Authorized", "username or password are wrong"));
		}
		
	}
}
