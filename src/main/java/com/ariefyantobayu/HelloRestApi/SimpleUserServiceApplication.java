package com.ariefyantobayu.HelloRestApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleUserServiceApplication.class, args);
	}

}
