package com.ariefyantobayu.HelloRestApi.response;

import org.springframework.http.HttpStatus;

import com.ariefyantobayu.HelloRestApi.handler.ResponseHandler;

public class ErrorResponse implements ResponseHandler {
	private int status;
	private String error;
	private String message;
	
	public int getStatus() {
		return status;
	}

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

	private ErrorResponse(int status, String error, String message) {
		super();
		this.status = status;
		this.error = error;
		this.message = message;
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, String error) {
		return new ErrorResponse(httpStatus.value(), error, "Don't have message attached");
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, String error, String message) {
		return new ErrorResponse(httpStatus.value(), error, message);
	}
}
