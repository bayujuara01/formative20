package com.ariefyantobayu.HelloRestApi.response;

import org.springframework.http.HttpStatus;

import com.ariefyantobayu.HelloRestApi.handler.ResponseHandler;

public class DataResponse implements ResponseHandler {
	private int status;
	private Object data;
	
	public int getStatus() {
		return status;
	}

	public Object getData() {
		return data;
	}

	private DataResponse(int status, Object data) {
		this.status = status;
		this.data = data;
	}
	
	public static DataResponse of(HttpStatus httpStatus, Object data) {
		return new DataResponse(httpStatus.value(), data);
	}
}
