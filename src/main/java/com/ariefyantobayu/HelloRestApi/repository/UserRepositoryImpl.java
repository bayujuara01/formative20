package com.ariefyantobayu.HelloRestApi.repository;

import java.math.BigInteger;
import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.ariefyantobayu.HelloRestApi.entity.User;

@Repository
public class UserRepositoryImpl {

	@Autowired
	EntityManager entityManager;

	@Transactional
	public Long saveUserReturnNative(String username, String password) {
		BigInteger lastInsertId = BigInteger.valueOf(-1L);
		
		Query query = entityManager.createNativeQuery(
				"INSERT INTO user(username, password, timestamp) VALUES (:username,:password,:timestamp);", User.class);
		query.setParameter("username", username);
		query.setParameter("password", password);
		query.setParameter("timestamp", LocalDateTime.now());
		query.executeUpdate();
		
		try {
			Query queryLastInsertId = entityManager.createNativeQuery("SELECT last_insert_id();");
			lastInsertId = (BigInteger) queryLastInsertId.getSingleResult();
		} catch (Exception e) {
			lastInsertId = BigInteger.valueOf(-1L);
		}
	
		return lastInsertId.longValue();
		
	}
}
