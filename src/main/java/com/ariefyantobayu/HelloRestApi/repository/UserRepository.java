package com.ariefyantobayu.HelloRestApi.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.HelloRestApi.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	@Query(
			value = "SELECT * FROM user",
			nativeQuery = true
			)
	List<User> findAllNative();
	
	@Query(value = "SELECT * FROM user WHERE id = ?1",
			nativeQuery = true)
	Optional<User> findByIdNative(Long userId);
	
	@Query(value = "SELECT * FROM user WHERE username = ?1",
			nativeQuery = true)
	Optional<User> findByUsernameNative(String username);
	
	@Query(value = "SELECT * FROM user WHERE username = ?1 AND password = ?2",
			nativeQuery = true)
	Optional<User> findByUsernameAndPassword(String username, String password);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO user(username, password) VALUES (?1,?2)", 
			nativeQuery = true)
	int saveNative(String username, String password);
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM user WHERE id = ?1",
			nativeQuery = true)
	int deleteUserByIdNative(Long id);
}
